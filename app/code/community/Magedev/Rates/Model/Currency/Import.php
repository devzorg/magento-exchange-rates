<?php
/**
 * Mage Dev Solutions
 *
 * @author      Oleg Batishchev <info@magedev.ru>
 * @namespace     magento-exchange-rates
 * @package     Magedev_Rates
 * @copyright   Copyright (c) 2014 Mage Dev Solutions (http://magedev.ru)
 */

class Magedev_Rates_Model_Currency_Import
{
    const CACHE_ID = 'exchange-rate';
    const CACHE_TAG = 'EXCHANGE_RATES';
    const LIFETIME = 3;
}