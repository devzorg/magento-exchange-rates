<?php
/**
 * European Central Bank fetch rates (fixed currencies)
 *
 * @author      Oleg Batishchev <info@magedev.ru>
 * @namespace     magento-exchange-rates
 * @package     Magedev_Rates
 * @copyright   Copyright (c) 2014 Mage Dev Solutions (http://magedev.ru)
 */

class Magedev_Rates_Model_Currency_Import_Ecb extends Mage_Directory_Model_Currency_Import_Abstract
{
    const URL = 'http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml';
    const BASE_CURR = 'EUR';
    protected $_messages = array();
    /**
     * HTTP client
     *
     * @var Varien_Http_Client
     */
    protected $_httpClient;

    public function __construct()
    {
        $this->_httpClient = new Varien_Http_Client();

        $this->cache = Mage::app()->getCache();
        if(Mage::app()->getBaseCurrencyCode() != self::BASE_CURR)
            Mage::getSingleton('adminhtml/session')->addNotice( Mage::helper('magedev_rates')->__('Service supports %s as base currency.', self::BASE_CURR) );
    }

    protected function _convert($currencyFrom, $currencyTo, $retry=0)
    {
        if($currencyFrom != self::BASE_CURR && $currencyTo != self::BASE_CURR) {
            return null;
        }

        $result = $this->cache->load(Magedev_Rates_Model_Currency_Import::CACHE_ID);
        if (!$result) {
            try {
                $result = $this->_httpClient
                    ->setUri(self::URL)
                    ->setConfig(array('timeout' => 3))
                    ->request('GET')
                    ->getBody();

            } catch(Exception $e){
                if( $retry == 0 ) {
                    $this->_convert($currencyFrom, $currencyTo, 1);
                } else {
                    $this->_messages[] = Mage::helper('magedev_rates')->__('Cannot retrieve rates.');
                    Mage::log($e->getTraceAsString(),null,'magedev_currency.log');
                }
            }
            $this->cache->save($result, Magedev_Rates_Model_Currency_Import::CACHE_ID, array(Magedev_Rates_Model_Currency_Import::CACHE_TAG),Magedev_Rates_Model_Currency_Import::LIFETIME);
        }

        $xml = simplexml_load_string($result, null, LIBXML_NOERROR);
        if( !$xml ) {
            $this->_messages[] = Mage::helper('magedev_rates')->__('Cannot retrieve rates.');
            return null;
        }

        $rates = $xml->Cube->Cube->children();

        foreach( $rates as $item )
        {
            $arr = end($item);
            if($arr['currency'] == $currencyTo)
                return (float)$arr['rate'];
            if($arr['currency'] == $currencyFrom && $currencyTo == self::BASE_CURR)
                return round(1/(float)$arr['rate'],4,PHP_ROUND_HALF_UP);
        }
        return false;
    }

}