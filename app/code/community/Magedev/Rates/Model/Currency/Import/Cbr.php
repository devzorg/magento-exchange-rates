<?php
/**
 * Get currency from Central Bank of Russia
 *
 * @author      Oleg Batishchev <info@magedev.ru>
 * @namespace     magento-exchange-rates
 * @package     Magedev_Rates
 * @copyright   Copyright (c) 2014 Mage Dev Solutions (http://magedev.ru)
 */

class Magedev_Rates_Model_Currency_Import_Cbr extends Mage_Directory_Model_Currency_Import_Abstract
{
    const URL = 'http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL';
    var $cache = null;
    const BASE_CURR = 'RUB';
    protected $_messages = array();

    public function __construct()
    {
        $this->cache = Mage::app()->getCache();
        if(Mage::app()->getBaseCurrencyCode() != self::BASE_CURR)
            Mage::getSingleton('adminhtml/session')->addNotice( Mage::helper('magedev_rates')->__('Service supports %s as base currency.', self::BASE_CURR) );
    }

    protected function _convert($currencyFrom, $currencyTo, $retry=0)
    {
        if($currencyFrom != self::BASE_CURR && $currencyTo != self::BASE_CURR) {
            return null;
        }

        $result = $this->cache->load(Magedev_Rates_Model_Currency_Import::CACHE_ID);
        if (!$result) {
            $soap = new SoapClient(self::URL);
            try {
                $response = $soap->GetCursOnDateXML(array('On_date' => date('Y-m-d')));
            } catch(SoapFault $e){
                $this->_messages[] = $e->getMessage();
                Mage::log($e->getTraceAsString(),null,'magedev_currency.log');
                return false;
            } catch(Exception $e){
                $this->_messages[] = $e->getMessage();
                Mage::log($e->getTraceAsString(),null,'magedev_currency.log');
                return false;
            }
            $this->cache->save($response->GetCursOnDateXMLResult->any, Magedev_Rates_Model_Currency_Import::CACHE_ID, array(Magedev_Rates_Model_Currency_Import::CACHE_TAG),Magedev_Rates_Model_Currency_Import::LIFETIME);
            $result = $response->GetCursOnDateXMLResult->any;
        }
        $xml = simplexml_load_string($result, null, LIBXML_NOERROR);
        if( !$xml ) {
            $this->_messages[] = Mage::helper('magedev_rates')->__('Cannot retrieve rates.');
            return null;
        }

        foreach($xml->ValuteCursOnDate as $item)
        {
            if((string)$item->VchCode == $currencyTo)
                return round(1/(float)$item->Vcurs,4,PHP_ROUND_HALF_UP);
            if((string)$item->VchCode == $currencyFrom && $currencyTo == self::BASE_CURR)
                return (float)$item->Vcurs;
        }
        return false;
    }
}